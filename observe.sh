#/bin/bash

function run_node {
    killall node
    yarn serve &
}



current=$(git diff)

run_node

while [ 1 ]
do
    if [ "$(git diff)" == "$current" ]
    then
        sleep 10
    else
        echo -e "\e[33mUpdating (new changes in repo)\e[0m"
        run_node
        current=$(git diff)
    fi
done
