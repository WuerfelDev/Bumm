// Organizes Games

const util = require('./util.js');

const Game = require('./game.js');
const Lobby = require('./lobby.js');

module.exports = class {

    constructor(io){
        this.io = io;
        this.activeGames = {};
        this.activeLobbies = {}; 
    }
    
    joinLobby(gamecode, username){
        if(this.lobbyExists(gamecode)){
            return this.activeLobbies[gamecode].newPlayer(username);
        }
    }
    
    createLobby(){
        let code;
        do{
            code = util.createToken(6, "abcdefghijklmnopqrstuvwxyz0123456789");
        }while(this.lobbyExists(code)||this.gameExists(code))
        this.activeLobbies[code] = new Lobby(this, code);
        return code;
    }
    
    lobbyExists(code){
        return code in this.activeLobbies;
    }
    
    
    gameExists(code){
        return code in this.activeGames;
    }
    
    lobbyToGame(code){
        let players = this.activeLobbies[code].players.filter(player => player.ready); //not yet testet but should be fine, right?
        this.activeGames[code] = new Game(this, code, players);
        this.activeLobbies[code].kill();
    }
    
    gameToLobby(code){ // Idea: start next/new game with same people / same code
        delete this.activeGames[code];
        this.activeLobbies[code] = new Lobby(this, code);
    }
    
    
    //XXX debug
    debug(){
        return {
            lobbies: Object.keys(this.activeLobbies),
            games: Object.keys(this.activeGames)
            };
    }

}
