// Coordinates the game

const util = require('./util.js');

module.exports = class {
    
    constructor(callback, gamecode, players){
        this.parent = callback;
        this.gamecode = gamecode;
        this.players = players;
        
        this.createGame(this);
    }
    
    //XXX use 'here.' instead of 'this.' for functions in this class
    // Its a workaround, I can't fix it
    createGame(here){
        console.log("Game '"+this.gamecode+"' created");
    }
    
    
}
