// Utilities

module.exports = {

    createToken(length, charPool) {
        let token = "";
        for (let i = 0; i < length; i++) {
            token += charPool[Math.floor(Math.random() * Math.floor(charPool.length))]
        }
        return token;
    },
    
    
    // https://stackoverflow.com/a/36499839/4346956
    killNamespace(io, namespacename) {
        let MyNamespace = io.of(namespacename); // Get Namespace
        for(let socketId in MyNamespace.connected){
            MyNamespace.connected[socketId].disconnect(); // Disconnect Each socket
        }
        MyNamespace.removeAllListeners(); // Remove all Listeners for the event emitter
        delete io.nsps[namespacename]; // Remove from the server namespaces
    },

}
