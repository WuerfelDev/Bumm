// Player class

const util = require('./util.js');

module.exports = class {

    constructor(username){
        this.username = username;
        this.token = util.createToken(16, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
        this.inLobby = false;
        this.ready = false;
    }
    
    
    reset(){
        this.inLobby = false;
        this.ready = false;
    }
    
    getStatus(){
        return {
            name: this.username,
            inLobby: this.inLobby,
            ready: this.ready
            };
    }

}
